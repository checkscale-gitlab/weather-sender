# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from typing import List, Optional
from pydantic import BaseSettings, EmailStr
from pydantic.fields import Field


class SecuritySettings(BaseSettings):
    api_key: Optional[str] = Field(
        description="Optional API key which should be used to perform security check."
        " If API key is not specified, no security check will be performed."
    )

    class Config:
        env_prefix = "security_"
        env_file = ".env"


class OpenWeatherSettings(BaseSettings):
    """OpenWeather is the service from which weather forecasts are downloaded."""

    api_key: str = Field(
        description="API key used to get weather data with OpenWeather."
    )
    base_url: str = Field(
        "https://api.openweathermap.org", description="OpenWeather API base URL."
    )

    class Config:
        env_prefix = "openweather_"
        env_file = ".env"

        # Following configuration generates an hashable model,
        # which is required in order to cache health check dependency.
        allow_mutation = False
        frozen = True


class TelegramSettings(BaseSettings):
    """Telegram is used to send messages with weather forecasts."""

    bot_token: str = Field(description="Bot token used to send messages with Telegram.")
    disable_notification: bool = Field(
        True,
        description="Controls whether sent messages should trigger a full notification."
        " Even if set to false, only the first message will trigger a full notification."
        " Defaults to true, which means that messages will trigger a silent notification.",
    )

    class Config:
        env_prefix = "telegram_"
        env_file = ".env"


class SendGridSettings(BaseSettings):
    """SendGrid is used to send emails with details about service errors."""

    api_key: Optional[str] = Field(
        description="API key used to send emails with SendGrid."
    )
    from_email: Optional[EmailStr] = Field(
        description="Email address from which error messages are sent."
    )
    to_emails: List[EmailStr] = Field(
        [], description="Email addresses to which error messages are sent"
    )

    class Config:
        env_prefix = "sendgrid_"
        env_file = ".env"


class HealthCheckSettings(BaseSettings):
    ping_timeout: int = Field(
        2,
        description="How many seconds should OpenWeather API ping last before failing."
        " Default value is 2 seconds, timeout value should be greater than zero.",
        gt=0,
    )

    class Config:
        env_prefix = "health_check_"
        env_file = ".env"

        # Following configuration generates an hashable model,
        # which is required in order to cache health check dependency.
        allow_mutation = False
        frozen = True
