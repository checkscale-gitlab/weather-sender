# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from enum import Enum
from typing import Optional
from pydantic import BaseModel, Field


class Units(str, Enum):
    IMPERIAL = "imperial"
    METRIC = "metric"


class ForecastRequest(BaseModel):
    """Models the information required to send a weather forecast."""

    latitude: float = Field(description="Latitude.")
    longitude: float = Field(description="Longitude.")
    language: str = Field(
        "en",
        description="Language that will be used to translate weather descriptions"
        " and labels contained in the forecast.",
    )
    units: Units = Field(
        Units.METRIC, description="Units of measurement. Default value is 'metric'."
    )
    chat_id: int = Field(
        description="Telegram chat ID to which the forecast will be sent."
    )
    location_name: Optional[str] = Field(
        None,
        description="Optional location name,"
        " which will override the one found using reverse geocoding.",
    )


class ForecastRequests(BaseModel):
    """Models the information required to send multiple weather forecast."""

    requests: list[ForecastRequest] = Field([], description="Requests.")
    background: bool = Field(
        False,
        description="If true, forecasts will be sent after the response"
        " has been returned to the caller. This parameter should be set to true"
        " when many requests should handled and/or there is a strict timeout"
        " for HTTP responses. When true, response array will be empty.",
    )


class ForecastSourceData(BaseModel):
    """Source data used to send the forecast."""

    weather: dict = Field(description="Weather data.")
    location: dict = Field(description="Location data.")
    air_pollution: dict = Field(description="Air pollution data.")


class ForecastTransformedData(BaseModel):
    """Transformed data used to send the forecast."""

    weather: dict = Field(description="Weather data.")


class ForecastResult(BaseModel):
    """Models the output of a single forecast request."""

    source_data: ForecastSourceData = Field(description="Source data.")
    transformed_data: ForecastTransformedData = Field(description="Transformed data.")


class ForecastResults(BaseModel):
    """Models the output of multiple forecast requests."""

    results: list[ForecastResult] = Field([], description="Results.")


# pylint: disable=too-many-instance-attributes
# Hourly weather requires all that information.
class HourlyDatum:
    """Internal model used to track weather information for an hour."""

    aqi: int
    hour: str
    humidity: int
    prec: float
    temp: float
    uvi: float
    weather_description: str
    weather_icon: str
    wind_speed: float


# pylint: disable=too-many-instance-attributes
# Daily weather requires all that information.
class DailyDatum:
    """Internal model used to track weather information for a day."""

    day: str
    humidity: int
    moon_phase: float
    pop: float
    prec: float
    temp_min: float
    temp_max: float
    weather_description: str
    weather_icon: str
    wind_deg: float
    wind_direction: str
    wind_direction_short: str
    wind_gust: Optional[float]
    wind_speed: float


# pylint: disable=too-many-instance-attributes
# Alerts requires all that information.
class AlertDatum:
    """Internal model used to track alert information."""

    end_day: str
    end_hour: str
    end_minute: str
    event: str
    icon: str
    sender_name: str
    start_day: str
    start_hour: str
    start_minute: str
