# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
import tempfile
import traceback
from typing import Iterable

import sendgrid
import telegram
from sendgrid.helpers.mail import Content, Email, Mail, To

from weather_sender.models import ForecastRequest
from weather_sender.settings import SendGridSettings, TelegramSettings
from weather_sender.templating import (
    AIR_TEMPLATE,
    MESSAGE_TEMPLATE,
    TODAY_TEMPLATE,
    TONIGHT_TEMPLATE,
    WEEK_TEMPLATE,
    convert_html_to_image,
    render_template,
)


def __create_tmp_html():
    return tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", suffix=".html")


def __create_tmp_image():
    return tempfile.NamedTemporaryFile(suffix=".jpg")


def __send_telegram_message(
    telegram_settings: TelegramSettings, chat_id: int, message: str, images: Iterable
):
    bot = telegram.Bot(token=telegram_settings.bot_token)
    bot.send_message(
        chat_id,
        text=message,
        parse_mode=telegram.ParseMode.HTML,
        disable_notification=telegram_settings.disable_notification,
    )
    bot.send_media_group(
        chat_id,
        media=[telegram.InputMediaPhoto(img) for img in images],
        # Only the first message should trigger a notification.
        disable_notification=True,
    )


def send_telegram_message(telegram_settings, chat_id, weather_data):
    # Prepare Telegram text message.
    message = render_template(MESSAGE_TEMPLATE, weather_data)

    # Prepare forecast images and send them with the text message.
    with (
        __create_tmp_html() as today_html,
        __create_tmp_image() as today_image,
        __create_tmp_html() as tonight_html,
        __create_tmp_image() as tonight_image,
        __create_tmp_html() as week_html,
        __create_tmp_image() as week_image,
        __create_tmp_html() as air_html,
        __create_tmp_image() as air_image,
    ):
        for tpl in [
            (TODAY_TEMPLATE, today_html, today_image),
            (TONIGHT_TEMPLATE, tonight_html, tonight_image),
            (WEEK_TEMPLATE, week_html, week_image),
            (AIR_TEMPLATE, air_html, air_image),
        ]:
            html = render_template(tpl[0], weather_data)
            convert_html_to_image(html, tpl[1], tpl[2])

        images = [today_image, tonight_image, week_image, air_image]
        __send_telegram_message(telegram_settings, chat_id, message, images)


def send_error_email(
    sendgrid_settings: SendGridSettings,
    logger: logging.Logger,
    request: ForecastRequest,
    exception: Exception,
):
    if (
        not sendgrid_settings.api_key
        or not sendgrid_settings.from_email
        or len(sendgrid_settings.to_emails) == 0
    ):
        logger.warning(
            "Error email could not be sent, not all SendGrid settings"
            " have been configured properly."
        )
        return

    sg_client = sendgrid.SendGridAPIClient(api_key=sendgrid_settings.api_key)

    from_email = Email(sendgrid_settings.from_email, "Weather Sender")
    to_emails = [To(to_email) for to_email in sendgrid_settings.to_emails]

    subject = f"Weather forecast for ({request.latitude}, {request.longitude}) could not be sent"

    body = (
        "An error occurred while sending weather forecast "
        f"for ({request.latitude}, {request.longitude}), "
        f"with language set to '{request.language}', "
        f"units of measurement set to '{request.units.value}', "
        f"custom location name set to '{request.location_name}' "
        f"and Telegram chat ID set to '{request.chat_id}'.\n\n"
    )

    exception_details = traceback.format_exception(exception)
    for exception_detail in exception_details:
        body += exception_detail

    mail = Mail(
        from_email,
        to_emails,
        subject,
        plain_text_content=Content("text/plain", body),
    )

    sg_client.send(mail)
