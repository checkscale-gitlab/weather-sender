# Frequently asked questions

## Weather forecast

### Where do forecasts come from?

Forecasts are produced by [OpenWeather][openweather-website]
and they are consumed using their APIs:

- [Weather forecast][openweather-one-call-api]
- [Air pollution forecast][openweather-air-pollution-api]
- [Geo data][openweather-geocoding-api]

### Which data is shown in the Telegram message?

![Telegram message][images-message]

Assuming the message is sent at 7 AM,
the information shown is as follows:

- The sum of the precipitation forecast for the day (from 7 AM to 11 PM).
  - If the sum is zero, the "no" label is shown.
  - In case the sum is a number very close to zero,
    the label "no or light" is shown.
  - Otherwise, the sum is followed by the probability of precipitation.
- The minimum and maximum temperature for the day (between 7 AM and 11 PM).
- The minimum humidity for the day (between 7 AM and 11 PM). The minimum value is shown
  because OpenWeather daily forecasts show that figure and it was decided to
  proceed in the same way.
- The maximum speed reached by gusts of wind during the day (between 7 AM and 11 PM).
- If present, the weather warnings issued by local authorities.

### What data is shown in the hourly tables?

![Forecast for today][images-today]
![Forecast for tonight][images-tonight]

For the 24 hours subsequent to sending the message,
the information shown is as follows:

- Weather forecast.
- Temperature.
- Average wind speed.
- Humidity.
- Amount of precipitation (rain and/or snow).
- UV index. See the dedicated question.
- Air quality index. See the dedicated question.

### What data is shown in the daily table?

![Forecast for next week][images-week]

For the seven days following the one in which the message was sent,
the information shown is as follows:

- Weather forecast.
- Maximum and minimum temperature.
- Average wind speed and direction.
- Humidity.
- Amount of precipitation (rain and / or snow).
- Moon phase. See the dedicated question.

### What data is shown in the air quality table?

![Air quality forecast][images-air]

For the 24 hours subsequent to sending the message,
the information shown is the following, grouped in bi-hourly rows:

- Air quality index. See the dedicated question.
- Quantity of pollutants.

### What does the UV indicator show?

The UV index is shown as a sun that can take on the following colors:

- Gray if the index is less than 1.
- Green if the index is less than 3.
- Orange if the index is less than 8.
- Red if the index is equal to or greater than 8.

It is recommended that you read the instructions in this
[Wikipedia article][wikipedia-uv-index] and it is recommended to apply
adequate protections in the presence of the orange or red indicator.

### What does the air quality indicator show?

The data received from the [OpenWeather service][openweather-air-pollution-api]
report the values of the [CAQI index][wikipedia-air-quality-index-caqi],
which are shown as a balloon that can take on the following colors:

- Green if the index is less than or equal to 50.
- Orange if the index is less than or equal to 100.
- Red if the index is above 100.

### How is the moon phase represented?

The moon phase is represented with the following iconography:

- A black circle for the new moon.
- An upward arrow for the crescent moon.
- An illuminated crescent on the right for the first quarter in the northern hemisphere,
  while the crescent is illuminated on the left for the southern hemisphere.
- A white circle for the full moon.
- A down arrow for the waning moon.
- An illuminated crescent on the left for the last quarter in the northern hemisphere,
  while the crescent is illuminated on the right for the southern hemisphere.

## Contacts

### I have some ideas for this project. How do I share them?

I can [open an issue][project-new-issue] on this GitLab project.
Otherwise, if you do not have a GitLab account or if you do not know to use GitLab,
then you can send an email to this address:

`contact-project+pommalabs-weather-sender-32489905-issue-@incoming.gitlab.com`

### I have some change requests. Can I share them?

Sure, using the channels described above. However, it is likely that only
small change request will be considered: if you want to customize sent messages,
you can freely modify project sources according to the [license][project-license].

[images-air]: https://alessioparma.xyz/images/weather-sender/air.jpg
[images-message]: https://alessioparma.xyz/images/weather-sender/message.png
[images-today]: https://alessioparma.xyz/images/weather-sender/today.jpg
[images-tonight]: https://alessioparma.xyz/images/weather-sender/tonight.jpg
[images-week]: https://alessioparma.xyz/images/weather-sender/week.jpg
[openweather-air-pollution-api]: https://openweathermap.org/api/air-pollution
[openweather-geocoding-api]: https://openweathermap.org/api/geocoding-api
[openweather-one-call-api]: https://openweathermap.org/api/one-call-api
[openweather-website]: https://openweathermap.org/
[project-license]: https://gitlab.com/pommalabs/weather-sender/-/blob/main/LICENSE
[project-new-issue]: https://gitlab.com/pommalabs/weather-sender/-/issues/new
[wikipedia-air-quality-index-caqi]: https://en.wikipedia.org/wiki/Air_quality_index#CAQI
[wikipedia-uv-index]: https://en.wikipedia.org/wiki/Ultraviolet_index
